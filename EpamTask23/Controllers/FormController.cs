﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EpamTask23.Controllers
{

    public class FormController : Controller
    {
        public static string[] selects = new string[] { "Select1", "Select4", "Select3", "Select2" };
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Select = (selects.ToList(), "example_select");
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            ViewBag.Name = form[0];
            ViewBag.CheckedSelects = form[2].Split(',');
            ViewBag.Radio = form[3];
            return View("Results");
        }
    }
}